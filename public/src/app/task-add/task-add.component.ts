import { Component } from '@angular/core';
import { TaskService } from '../task.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.css']
})
export class TaskAddComponent {

  constructor(private task: TaskService,
    private router: Router,
    private aroute: ActivatedRoute) { }


  onClickSubmit(data) {
    this.task.postTask(data).subscribe(() => {
      this.router.navigate(['/listTask']);
    });
  }

}
