import { Injectable } from '@angular/core';
import { HttpHeaders, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Task } from './task';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private baseUrl = 'http://localhost:8080/api/task/';
  private headers = new HttpHeaders({'Content-Type': 'application/json'});
  private options = {headers: this.headers};

  constructor(private _http: HttpClient) { }

  getTasks(): Observable<Task[]> {
    return this._http.get<Task[]> (this.baseUrl).pipe(
      catchError(this.errorHandler));
  }

  getTaskById(id: number): Observable<Task> {
    return this._http.get<Task>(this.baseUrl + '/' + id, this.options);
  }

  postTask(task: Task): Observable<Task> {
    return this._http.post<Task>(this.baseUrl + '/',  JSON.stringify(task), this.options);
  }

  updateTasks(task: Task): Observable<Task> {
    return this._http.put<Task> (this.baseUrl + '/' + task.id, JSON.stringify(task), this.options);
  }

  deleteTask(id: number): Observable<Task> {
    return this._http.delete<Task> (this.baseUrl + '/' + id, this.options);
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError(error.message || 'Server-Error');
  }

}
